package com.training.library.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.Optional;
import com.training.library.doa.ItemMongoCol;
import com.training.library.model.Item;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.training.library.model.Type;

@SpringBootTest
public class LibraryServiceTests {

    @Autowired
    LibraryService libraryService;

    @MockBean
    ItemMongoCol itemMongoColMock;
    Item item = new Item("booky", "data", Type.BOOK, false);

    @Test
    public void test_addItems() {
        when(itemMongoColMock.insert(item)).thenReturn(item);
        item = libraryService.add(item);

    }

    @Test
    public void test_borrowItem() {
        Item itemWithId = item.clone();
        itemWithId.setId("a1");
        when(itemMongoColMock.findByTitle(item.getTitle())).thenReturn(itemWithId);
        when(itemMongoColMock.save(itemWithId)).thenReturn(itemWithId);
        Item borrowed = libraryService.borrow("booky");
        verify(itemMongoColMock).save(itemWithId);
        assertEquals(itemWithId.isBorrowed(), borrowed.isBorrowed());
    }

    @Test
    public void test_removeItem() {
        Item item = new Item("booky", "data", Type.BOOK, false);
        libraryService.remove(item);
    }

    @Test
    public void test_returnItem() {
        Item itemWithId = item.clone();
        itemWithId.setBorrowed(true);
        itemWithId.setId("a1");
        when(itemMongoColMock.findById(itemWithId.getId())).thenReturn(Optional.of(itemWithId));
        boolean ans = libraryService.returnItem(itemWithId);
        verify(itemMongoColMock).findById(itemWithId.getId());
        assertEquals(ans,true);
        
    }
}