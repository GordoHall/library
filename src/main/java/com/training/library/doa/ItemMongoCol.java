package com.training.library.doa;

import com.training.library.model.Item;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemMongoCol extends MongoRepository<Item,String>{

	Item findByTitle(String title);

}