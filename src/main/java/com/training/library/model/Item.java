package com.training.library.model;

public class Item {
    
   private String id = "";  
    private Type type;
    private boolean isBorrowed;
    private String title;
    private String dateTime;
 
    public Item(String title, String dateTime, Type type, boolean isBorrowed) {
        this.title = title;
        this.dateTime = dateTime;
        this.type = type;
        this.isBorrowed = isBorrowed;
       

    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean isBorrowed) {
        this.isBorrowed = isBorrowed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Item [+title=" + title +", isBorrowed=" + isBorrowed + ", type=" + type
                + "]";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public Item clone(){
        return new Item(this.title,this.dateTime,this.type,this.isBorrowed);
    }

}
