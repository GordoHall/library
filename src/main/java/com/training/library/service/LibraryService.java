package com.training.library.service;

import java.util.HashMap;
import java.util.Optional;

import com.training.library.doa.ItemMongoCol;
import com.training.library.model.Item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * Provides Libray services
 * @see Item
 * @author Gordon Hall
 */
@Component
public class LibraryService implements LibraryInterface{

    public static final Logger LOG = LoggerFactory.getLogger(LibraryService.class);

    @Autowired
    private ItemMongoCol itemMongoCol;

  
    public Item add(Item item) {
        LOG.info("adding Item");
        Item ans = itemMongoCol.insert(item);
        return ans;
    }

    public void remove(Item item) {
        itemMongoCol.delete(item);
    }

    public Item borrow(String title) {
        Item ans = itemMongoCol.findByTitle(title);
        ans.setBorrowed(true);
        ans = itemMongoCol.save(ans);
        return ans;

    }

    public boolean returnItem(Item item) {
        Optional<Item> ans = itemMongoCol.findById(item.getId());
        return ans.isPresent();
    }

    public Item search(String id) {
        Optional<Item> ans = itemMongoCol.findById(id);
        return (Item) ans.get();
    }

}