package com.training.library.service;

import com.training.library.model.Item;

public interface LibraryInterface {
    /**
     * Add to library
     * 
     * @param item
     * @return Item
     */
    Item add(Item item);

    /**
     * Remove from library
     * 
     * @param item
     * @return Item
     */
    void remove(Item item);

    /**
     * Borrow library
     * 
     * @param item
     * @return Item
     */
    public Item borrow(String title);

    /**
     * Return Item to library
     * 
     * @param item
     * @return boolean
     */

    public boolean returnItem(Item item);

    /**
     * Search the library
     * 
     * @param item
     * @return Item
     */
    public Item search(String id);
}
